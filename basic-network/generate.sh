export PATH=$GOPATH/src/github.com/hyperledger/fabric/build/bin:${PWD}/../bin:${PWD}:$PATH
export FABRIC_CFG_PATH=${PWD}

CHANNEL_BOTH=channelboth
CHANNEL_BOTH1=channelboth1
CHANNEL_UIDAI=channeluidai
CHANNEL_DL=channeldl
CHANNEL_TAXI=channeltaxi
CHANNEL_COMMON=channelcommon


# remove previous crypto material and config transactions
# rm -fr config/*
# rm -fr crypto-config/*

# generate crypto material
echo

echo 
echo "#####################################################################"
echo "#############  Generating Cryptographic Materials  ##################"
echo "#####################################################################"
echo
../bin/cryptogen generate --config=./crypto-config.yaml
if [ "$?" -ne 0 ]; then
  echo "Failed to generate crypto material..."
  exit 1
fi
sleep 2

echo
echo "#####################################################################"
echo "#############          Creating Genesis Block      ##################"
echo "#####################################################################"
echo
../bin/configtxgen -profile OrdererGenesis -outputBlock ./config/genesis.block
if [ "$?" -ne 0 ]; then
  echo "Failed to generate orderer genesis block..."
  exit 1
fi
echo
sleep 2

echo
echo "###########################################################################"
echo "##### Generating channel configuration transaction 'commonchannel.tx' ######"
echo "###########################################################################"
echo
../bin/configtxgen -profile CommonChannelOrgs -outputCreateChannelTx ./config/commonchannel.tx -channelID $CHANNEL_COMMON
if [ "$?" -ne 0 ]; then
  echo "Failed to generate channel configuration transaction..."
  exit 1
fi
sleep 2

echo
echo "###########################################################################"
echo "##### Generating channel configuration transaction 'channeluidai.tx' ######"
echo "###########################################################################"
echo
../bin/configtxgen -profile ChannelOrgUidai -outputCreateChannelTx ./config/channeluidai.tx -channelID $CHANNEL_UIDAI
if [ "$?" -ne 0 ]; then
  echo "Failed to generate channel configuration transaction..."
  exit 1
fi
sleep 2

echo
echo "###########################################################################"
echo "##### Generating channel configuration transaction 'channeldl.tx' ######"
echo "###########################################################################"
echo
../bin/configtxgen -profile ChannelOrgDl -outputCreateChannelTx ./config/channeldl.tx -channelID $CHANNEL_DL
if [ "$?" -ne 0 ]; then
  echo "Failed to generate channel configuration transaction..."
  exit 1
fi
sleep 2
echo
echo "###########################################################################"
echo "##### Generating channel configuration transaction 'channeltaxi.tx' ######"
echo "###########################################################################"
echo
../bin/configtxgen -profile ChannelOrgTaxi -outputCreateChannelTx ./config/channeltaxi.tx -channelID $CHANNEL_TAXI
if [ "$?" -ne 0 ]; then
  echo "Failed to generate channel configuration transaction..."
  exit 1
fi
sleep 2

echo
echo "###########################################################################"
echo "##### Generating channel configuration transaction 'channelboth.tx' ######"
echo "###########################################################################"
echo
../bin/configtxgen -profile ChannelBothOrgs -outputCreateChannelTx ./config/channelboth.tx -channelID $CHANNEL_BOTH
if [ "$?" -ne 0 ]; then
  echo "Failed to generate channel configuration transaction..."
  exit 1
fi
sleep 2

echo
echo "###########################################################################"
echo "##### Generating channel configuration transaction 'channelboth1.tx' ######"
echo "###########################################################################"
echo
../bin/configtxgen -profile ChannelBothOrgs1 -outputCreateChannelTx ./config/channelboth1.tx -channelID $CHANNEL_BOTH1
if [ "$?" -ne 0 ]; then
  echo "Failed to generate channel configuration transaction..."
  exit 1
fi
sleep 2

echo
echo "############################################################################"
echo "####### Defining Anchor Peers for Orgtaxi ORG for commonchannel #############"
echo "############################################################################"
echo
../bin/configtxgen -profile CommonChannelOrgs -outputAnchorPeersUpdate ./config/OrgtaxiMSPanchors.tx -channelID $CHANNEL_COMMON -asOrg OrgtaxiMSP
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for OrguidaiMSP..."
  exit 1
fi
sleep 2

echo
echo "############################################################################"
echo "####### Defining Anchor Peers for Orguidai ORG for commonchannel #############"
echo "############################################################################"
echo
../bin/configtxgen -profile CommonChannelOrgs -outputAnchorPeersUpdate ./config/OrgdlMSPanchors.tx -channelID $CHANNEL_COMMON -asOrg OrguidaiMSP
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for OrguidaiMSP..."
  exit 1
fi
sleep 2

echo
echo "############################################################################"
echo "####### Defining Anchor Peers for Orguidai ORG for bothchannel #############"
echo "############################################################################"
echo
../bin/configtxgen -profile ChannelBothOrgs -outputAnchorPeersUpdate ./config/OrguidaiMSPanchors.tx -channelID $CHANNEL_BOTH -asOrg OrguidaiMSP
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for OrguidaiMSP..."
  exit 1
fi
sleep 2

echo
echo "#########################################################################"
echo "##### Defining Anchor Peers for Orgtaxi ORG for bothchannel##############"
echo "#########################################################################"
echo
../bin/configtxgen -profile ChannelBothOrgs -outputAnchorPeersUpdate ./config/OrgdlMSPanchors.tx -channelID $CHANNEL_BOTH -asOrg OrgtaxiMSP
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for OrgtaxiMSP..."
  exit 1
fi
echo
sleep 2

echo
echo "#########################################################################"
echo "###### Defining Anchor Peers for Orgtaxi ORG for bothchannel1  ##########"
echo "#########################################################################"
echo
../bin/configtxgen -profile ChannelBothOrgs1 -outputAnchorPeersUpdate ./config/OrgdlMSPanchors.tx -channelID $CHANNEL_BOTH1 -asOrg OrgtaxiMSP
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for OrgtaxiMSP..."
  exit 1
fi
echo
sleep 2

echo
echo "#########################################################################"
echo "################# Defining Anchor Peers for Orgdl ORG  for bothchannel1##"
echo "#########################################################################"
echo
../bin/configtxgen -profile ChannelBothOrgs1 -outputAnchorPeersUpdate ./config/OrgdlMSPanchors.tx -channelID $CHANNEL_BOTH1 -asOrg OrgdlMSP
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for OrgdlMSP..."
  exit 1
fi
echo


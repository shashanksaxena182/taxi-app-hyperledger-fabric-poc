upgradeFabdlChaincode() {
    docker exec clidl peer chaincode install -n fabdl -v 1.1 -p github.com/fabdl -l golang
    docker exec clidl peer chaincode upgrade -n fabdl -v 1.1 -c '{"Args":[""]}' -p github.com/fabdl -C channeldl -l golang
}




# Parse commandline args
while getopts "h?m:o:a:w:c:0:1:2:3:k:v:i:n:M:I:R:P:" opt; do
  case "$opt" in
    h|\?)
      printHelp
      exit 0
    ;;
    m)  MODE=$OPTARG
    ;;
    v)  CHAINCODE_VERSION=$OPTARG
    ;;
    o)  ORG=$OPTARG
    ;;
    M)  MAIN_ORG=$OPTARG
    ;;
    a)  API_PORT=$OPTARG
    ;;
    w)  WWW_PORT=$OPTARG
    ;;
    c)  CA_PORT=$OPTARG
    ;;
    0)  PEER0_PORT=$OPTARG
    ;;
    1)  PEER0_EVENT_PORT=$OPTARG
    ;;
    2)  PEER1_PORT=$OPTARG
    ;;
    3)  PEER1_EVENT_PORT=$OPTARG
    ;;
    k)  CHANNELS=$OPTARG
    ;;
    i) IP=$OPTARG
    ;;
    n) CHAINCODE=$OPTARG
    ;;
    I) CHAINCODE_INIT_ARG=$OPTARG
    ;;
    R) REMOTE_ORG=$OPTARG
    ;;
    P) ENDORSEMENT_POLICY=$OPTARG
    ;;
  esac
done


if [ "${MODE}" == "upgradeFabdlChaincode" ]; then
    upgradeFabdlChaincode
elif [ "${MODE}" == "upgradeCommonChaincode" ]; then
    upgradeCommonChaincode 
elif [ "${MODE}" == "upgradeBilateralChaincode" ]; then
    upgradeBilateralChaincode   
else
  printHelp
  exit 1
fi
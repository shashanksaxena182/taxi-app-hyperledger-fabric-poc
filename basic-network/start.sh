bash generate.sh
docker-compose -f docker-compose.yaml down
docker-compose -f docker-compose.yaml up -d

sleep 3

echo
echo "##################################################"
echo "####### Creating channel 'common' ########"
echo "##################################################"
echo
docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orgtaxi.example.com/msp" peer0.orgtaxi.example.com peer channel create -o orderer.example.com:7050 -c channelcommon -f /etc/hyperledger/configtx/commonchannel.tx --outputBlock /etc/hyperledger/configtx/commonchannel.block
echo
sleep 2

echo
echo "##################################################"
echo "####### Creating channel 'channeluidai' ########"
echo "##################################################"
echo
docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orguidai.example.com/msp" peer0.orguidai.example.com peer channel create -o orderer.example.com:7050 -c channeluidai -f /etc/hyperledger/configtx/channeluidai.tx --outputBlock /etc/hyperledger/configtx/channeluidai.block
echo
sleep 2

echo
echo "##################################################"
echo "####### Creating channel 'channeldl' ########"
echo "##################################################"
echo
docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orgdl.example.com/msp" peer0.orgdl.example.com peer channel create -o orderer.example.com:7050 -c channeldl -f /etc/hyperledger/configtx/channeldl.tx --outputBlock /etc/hyperledger/configtx/channeldl.block
echo
sleep 2

echo
echo "##################################################"
echo "####### Creating channel 'channeltaxi' ########"
echo "##################################################"
echo
docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orgtaxi.example.com/msp" peer0.orgtaxi.example.com peer channel create -o orderer.example.com:7050 -c channeltaxi -f /etc/hyperledger/configtx/channeltaxi.tx --outputBlock /etc/hyperledger/configtx/channeltaxi.block
echo
sleep 2

echo
echo "##################################################"
echo "#######  Createing channel 'channelboth' ########"
echo "##################################################"
echo
docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orgtaxi.example.com/msp" peer0.orgtaxi.example.com peer channel create -o orderer.example.com:7050 -c channelboth -f /etc/hyperledger/configtx/channelboth.tx --outputBlock /etc/hyperledger/configtx/channelboth.block
echo
sleep 2

echo
echo "##################################################"
echo "#######  Createing channel 'channelboth1' ########"
echo "##################################################"
echo
docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orgtaxi.example.com/msp" peer0.orgtaxi.example.com peer channel create -o orderer.example.com:7050 -c channelboth1 -f /etc/hyperledger/configtx/channelboth1.tx --outputBlock /etc/hyperledger/configtx/channelboth1.block
echo
sleep 2


echo
echo "##############################################################################"
echo "#######  Joining Peers of Orgtaxi , Orguidai and Orgdl to channel 'common' ########"
echo "##############################################################################"
echo

docker exec -e "CORE_PEER_LOCALMSPID=OrguidaiMSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orguidai.example.com/msp" peer0.orguidai.example.com peer channel join -b /etc/hyperledger/configtx/commonchannel.block
sleep 5

echo
docker exec -e "CORE_PEER_LOCALMSPID=OrgtaxiMSP" -e  "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orgtaxi.example.com/msp" peer0.orgtaxi.example.com peer channel join -b /etc/hyperledger/configtx/commonchannel.block
sleep 5

echo
docker exec -e "CORE_PEER_LOCALMSPID=OrgdlMSP" -e  "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orgdl.example.com/msp" peer0.orgdl.example.com peer channel join -b /etc/hyperledger/configtx/commonchannel.block


echo
echo "##############################################################################"
echo "#######  Joining Peer of Orguidai to channel 'channeluidai' ########"
echo "##############################################################################"
echo

docker exec -e "CORE_PEER_LOCALMSPID=OrguidaiMSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orguidai.example.com/msp" peer0.orguidai.example.com peer channel join -b /etc/hyperledger/configtx/channeluidai.block
sleep 2

echo
echo "##############################################################################"
echo "#######  Joining Peer of Orgdl to channel 'channeldl' ########"
echo "##############################################################################"
echo

docker exec -e "CORE_PEER_LOCALMSPID=OrgdlMSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orgdl.example.com/msp" peer0.orgdl.example.com peer channel join -b /etc/hyperledger/configtx/channeldl.block
sleep 2

echo
echo "##############################################################################"
echo "#######  Joining Peer of Orgtaxi to channel 'channeltaxi' ########"
echo "##############################################################################"
echo

docker exec -e "CORE_PEER_LOCALMSPID=OrgtaxiMSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orgtaxi.example.com/msp" peer0.orgtaxi.example.com peer channel join -b /etc/hyperledger/configtx/channeltaxi.block
sleep 2

echo
echo "##############################################################################"
echo "#######  Joining Peers of Orgtaxi and Orguidai to channel 'channelboth' ########"
echo "##############################################################################"
echo

docker exec -e "CORE_PEER_LOCALMSPID=OrguidaiMSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orguidai.example.com/msp" peer0.orguidai.example.com peer channel join -b /etc/hyperledger/configtx/channelboth.block
sleep 5

echo
docker exec -e "CORE_PEER_LOCALMSPID=OrgtaxiMSP" -e  "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orgtaxi.example.com/msp" peer0.orgtaxi.example.com peer channel join -b /etc/hyperledger/configtx/channelboth.block
echo

echo
echo "##############################################################################"
echo "#######  Joining Peers of Orgtaxi and Orgdl to channel 'channelboth1' ########"
echo "##############################################################################"
echo

docker exec -e "CORE_PEER_LOCALMSPID=OrgtaxiMSP" -e  "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orgtaxi.example.com/msp" peer0.orgtaxi.example.com peer channel join -b /etc/hyperledger/configtx/channelboth1.block

sleep 5
echo
docker exec -e "CORE_PEER_LOCALMSPID=OrgdlMSP" -e  "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@orgdl.example.com/msp" peer0.orgdl.example.com peer channel join -b /etc/hyperledger/configtx/channelboth1.block
echo
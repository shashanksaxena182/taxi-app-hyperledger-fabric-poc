import { Component, OnInit, Output , EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Fabric_Response } from 'src/assets/data_structures';
import { PutStateService } from '../put-state.service'


@Component({
  selector: 'app-basic-info5',
  templateUrl: './basic-info5.component.html',
  styleUrls: ['./basic-info5.component.css']
})
export class BasicInfo5Component implements OnInit {

  response: Fabric_Response;
  uid: number;
  submitButton: Boolean = false;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private putStateService: PutStateService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(event => {
      this.uid = event.uid;
    });
    
  }

  form = new FormGroup({

    id:new FormControl(''),
    startloc:new FormControl('', Validators.required),
    endloc:new FormControl('', Validators.required),
    uid:new FormControl('', Validators.required),


  })


  fillbd3() {
    
    this.submitButton = true;

    this.response = { status: "Processing", message: "PROCESSING SUBMISSION..." };
    // this.router.navigateByUrl('uid'+this.uid)

    this.putStateService.taxibook(this.form.value)
      .then((res: Fabric_Response) => {
        if (res.status == "success") {
          res.message = "Click next to fetch details"
             this.response = res
        }
        else {
          this.response = res
        }
      })
  }

  next() {
    this.router.navigateByUrl('dl/fetchtaxiData/:booked_details'+this.form.controls['uid'].value)
  }
}

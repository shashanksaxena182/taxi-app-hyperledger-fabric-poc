import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicInfo5Component } from './basic-info5.component';

describe('BasicInfo5Component', () => {
  let component: BasicInfo5Component;
  let fixture: ComponentFixture<BasicInfo5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicInfo5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicInfo5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Fabric_Response } from 'src/assets/data_structures';
import { PutStateService } from '../put-state.service'


@Component({
  selector: 'app-basic-info3',
  templateUrl: './basic-info3.component.html',
  styleUrls: ['./basic-info3.component.css']
})
export class BasicInfo3Component implements OnInit {

  response: Fabric_Response;
  uid: number;
  submitButton: Boolean = false;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private putStateService: PutStateService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(event => {
      this.uid = event.uid;
    });
  }

  form = new FormGroup({

    id:new FormControl('', Validators.required),
    vehicletype:new FormControl('', Validators.required),
    numberplate:new FormControl('', Validators.required),
    carcompany:new FormControl('', Validators.required),
    carmake:new FormControl('', Validators.required),
    carcolour:new FormControl('', Validators.required),
    chasisnumber:new FormControl('', Validators.required),
    rto:new FormControl('', Validators.required),

  })

  fillbd3() {
    this.submitButton = true;
    this.response = { status: "Processing", message: "PROCESSING SUBMISSION..." };

    if(this.form.value.chasisnumber == '987654321' || this.form.value.chasisnumber == '987654320')
    {
      this.response = { status: "error", message: "Chasis Number is RTO rejected" };
      return;
    }
    if(this.form.value.rto == 'no')
    {
      this.response = { status: "error", message: "RTO non verified cabs can not be enrolled" };
      return;
    }
    this.putStateService.createVehicle(this.form.value)
      .then((res: Fabric_Response) => {
        if (res.status == "success") {
          res.message = "Application completed successfully."
             this.response = res
        }
        else {
          this.response = res
        }
      })
  }

  // next() {
  //   this.router.navigateByUrl('taxi/part3/' + this.uid)
  // }
}

import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Basic_Info_1, Basic_Info_2, Basic_Info_3, Basic_Info_4,Basic_Info_5,Basic_Info_6,Basic_Info_7,Fabric_Response } from '../assets/data_structures'
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PutStateService {

  constructor(private http: HttpClient) { }

  createUIDAI(data: Basic_Info_1): Promise<Fabric_Response> {
    let datastring = JSON.stringify(data)
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${environment.baseUrl}/api/uidai/part1/${datastring}`)
        .subscribe((data: Fabric_Response) => {
          resolve(data)
        })
    });
  }

  createTaxi(data: Basic_Info_4): Promise<Fabric_Response> {
    let datastring = JSON.stringify(data)
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${environment.baseUrl}/api/taxi/part1/${datastring}`)
        .subscribe((data: Fabric_Response) => {
          resolve(data)
        })
    });
  }
  
  createTaxiDriver(data: Basic_Info_5): Promise<Fabric_Response> {
    let datastring = JSON.stringify(data)
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${environment.baseUrl}/api/taxi/part2/${datastring}`)
        .subscribe((data: Fabric_Response) => {
          resolve(data)
        })
    });
  }

  createVehicle(data: Basic_Info_6): Promise<Fabric_Response> {
    let datastring = JSON.stringify(data)
    // let uidString = uid.toString();
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${environment.baseUrl}/api/taxi/part3/${datastring}`)
        .subscribe((data: Fabric_Response) => {
          resolve(data)
        })
    });
  }

  taxibook(data: Basic_Info_7): Promise<Fabric_Response> {
    let datastring = JSON.stringify(data)
    // let uidString = uid.toString();
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${environment.baseUrl}/api/taxi/part5/${datastring}`)
        .subscribe((data: Fabric_Response) => {
          resolve(data)
        })
    });
  }
  createUIDAI2(uid: number, data: Basic_Info_2): Promise<Fabric_Response> {
    let datastring = JSON.stringify(data)
    let uidString = uid.toString();
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${environment.baseUrl}/api/uidai/part2/${uid}/${datastring}`)
        .subscribe((data: Fabric_Response) => {
          resolve(data)
        })
    });
  }

  createUIDAI3(uid: number, data: Basic_Info_3): Promise<Fabric_Response> {
    let datastring = JSON.stringify(data)
    let uidString = uid.toString();
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${environment.baseUrl}/api/uidai/part3/${uid}/${datastring}`)
        .subscribe((data: Fabric_Response) => {
          resolve(data)
        })
    });
  }

  addRto(data: any): Promise<Fabric_Response> {
    let datastring = JSON.stringify(data)
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${environment.baseUrl}/api/dl/addRto/${datastring}`)
        .subscribe((data: Fabric_Response) => {
          resolve(data)
        })
    });
  }

  addOfficer(uid: number, rtoid: number): Promise<Fabric_Response> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${environment.baseUrl}/api/dl/addOfficer/${uid.toString()}/${rtoid.toString()}`)
        .subscribe((data: Fabric_Response) => {
          resolve(data)
        })
    });
  }

  applyLicense(uid: string): Promise<Fabric_Response> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${environment.baseUrl}/api/dl/applyLicense/${uid}`)
        .subscribe((data: Fabric_Response) => {
          resolve(data)
        })
    });
  }

  addScore(uid: string, scoretype: string, score: string, officerid: string): Promise<Fabric_Response> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${environment.baseUrl}/api/dl/addScore/${uid}/${scoretype}/${score}/${officerid}`)
        .subscribe((data: Fabric_Response) => {
          resolve(data)
        })
    });
  }

  addTicket(uid: string, officerid: string, reason: string, place: string, amount: string): Promise<Fabric_Response> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${environment.baseUrl}/api/dl/addTicket/${uid}/${officerid}/${reason}/${place}/${amount}`)
        .subscribe((data: Fabric_Response) => {
          resolve(data)
        })
    });
  }

  payFine(uid: string, ticketid: string): Promise<Fabric_Response> {
    return new Promise((resolve, reject) => {
      this.http.get<any>(`${environment.baseUrl}/api/dl/payFine/${uid}/${ticketid}`)
        .subscribe((data: Fabric_Response) => {
          resolve(data)
        })
    });
  }
}

import { Component, OnInit, Output , EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Fabric_Response } from 'src/assets/data_structures';
import { PutStateService } from '../put-state.service'


@Component({
  selector: 'app-basic-info4',
  templateUrl: './basic-info4.component.html',
  styleUrls: ['./basic-info4.component.css']
})
export class BasicInfo4Component implements OnInit {

  response: Fabric_Response;
  uid: number;
  submitButton: Boolean = false;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private putStateService: PutStateService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(event => {
      this.uid = event.uid;
    });
    
  }

  form = new FormGroup({

    uid:new FormControl('', Validators.required),
    
  })


  fillbd3() {
    
    this.submitButton = true;

    this.response = { status: "Processing", message: "PROCESSING SUBMISSION..." };
    this.router.navigateByUrl('dl/fetchData/:'+ this.form.controls['uid'].value)

    // this.putStateService.createVehicle(this.form.value)
    //   .then((res: Fabric_Response) => {
    //     if (res.status == "success") {
    //       res.message = "Click next to fetch details"
    //          this.response = res
    //     }
    //     else {
    //       this.response = res
    //     }
    //   })
  }

  // next() {
  //   this.router.navigateByUrl('taxi/dl/fetchData/:uid' + this.uid)
  // }
}

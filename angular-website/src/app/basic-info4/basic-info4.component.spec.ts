import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicInfo4Component } from './basic-info4.component';

describe('BasicInfo4Component', () => {
  let component: BasicInfo4Component;
  let fixture: ComponentFixture<BasicInfo4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicInfo4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicInfo4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

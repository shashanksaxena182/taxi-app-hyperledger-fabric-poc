import { Component, OnInit } from '@angular/core';
import { GetStateService } from '../get-state.service'
import { Fabric_Response, Basic_Info_8 } from 'src/assets/data_structures';
import { async, delay } from 'q';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-fetch-taxibook-data',
	templateUrl: './fetch-taxibook-data.component.html',
	styleUrls: ['./fetch-taxibook-data.component.css']
})
export class FetchtaxiDataComponent implements OnInit {

	uid: string;
	message: string;
	userData: Basic_Info_8;
	userDataString: string;
	fetchDataButtonDisabled: Boolean = false;
	IsFailed: Boolean = false;
	IsSuccess: Boolean = false;
	constructor(private getStateService: GetStateService, private activatedRoute: ActivatedRoute) { }

	ngOnInit() {
		this.activatedRoute.params
			.subscribe(event => {
				this.uid = event.uid;
				this.fetchData();
			});
	}
	// form = new FormGroup({

	// 	uid:new FormControl('', Validators.required),
	
	//   })

	async fetchData() {
		this.message = "PROCESSING DATA..."
		this.fetchDataButtonDisabled = true
		this.IsFailed = false;
		var uid =this.uid;
		var newid = uid.substr(1); 
		await this.getStateService.fetchtaxidata(newid)
		.then((res: Fabric_Response) => {
			                               this.IsSuccess = true
	                    	                this.userData = JSON.parse(res.message);
				                            this.userDataString = JSON.stringify(this.userData)
		                                    this.userData = this.userData;
		
		})
		// .then(async () => {

		// 		if (!this.IsFailed) {
		// 			// console.log("not working");
		// 			await this.getStateService.fetchtaxiDataFromDl(this.uid)
		// 				.then((res: Fabric_Response) => {
		// 					this.IsSuccess = true
		// 					this.userData = JSON.parse(res.message);
		// 					this.userDataString = JSON.stringify(this.userData)
		// 					this.uidData = this.userData.taxidata;
		// 				})
		// 				// .then(async (res: Fabric_Response) => {
		// 					// console.log("fetchtaxiDataToCommon" + JSON.stringify(res));
		// 					// if (res.status == "failed") {
		// 					// 	this.message = res.message
		// 					// 	this.fetchDataButtonDisabled = false
		// 					// 	this.IsFailed = true;
		// 					// }
		// 					//  if (res.status == "success") {

		// 								// this.getStateService.fetchtaxiDataFromDl(this.uid)
		// 								// 	.then((res: Fabric_Response) => {
		// 								// 		this.IsSuccess = true
		// 								// 		this.userData = JSON.parse(res.message);
		// 								// 		this.userDataString = JSON.stringify(this.userData)
		// 								// 		this.uidData = this.userData.taxidata;
		// 								// 	})
									
		// 					// }
		// 				// })
		// 		}
		// 	})
	}

	delay(ms: number) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}
}

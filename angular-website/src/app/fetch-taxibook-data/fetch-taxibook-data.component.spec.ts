import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FetchtaxiDataComponent } from './fetch-taxibook-data.component';

describe('FetchtaxiDataComponent', () => {
  let component: FetchtaxiDataComponent;
  let fixture: ComponentFixture<FetchtaxiDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FetchtaxiDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FetchtaxiDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

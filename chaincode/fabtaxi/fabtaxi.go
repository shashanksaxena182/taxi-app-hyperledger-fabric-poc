package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("fabtaxi")

type SimpleChainCode struct {
}

// SEPERATE DOCUMENT 1
// type taxiDetails struct {
// 	DocType         string          `json:"objectType"`
// 	ID              string          `json:"id"`
// 	BasicData_1     BasicInfo1      `json:"basicdata1"`
// 	BasicDriverData BasicInfoDriver `json:"basicdriverdata1"`

// 	// BasicData_2 BasicInfo2 `json:"basicdata2"`
// 	// AddressData Address    `json:"address"`
// 	IsActive string `json:"isactive"`
// }

type BasicInfo1 struct {
	ID            string `json:"id"`
	First_Name    string `json:"firstname"`
	Last_Name     string `json:"lastname"`
	Gender        string `json:"gender"`
	DOB           string `json:"dob"`
	Age           string `json:"age"`
	ContactNumber string `json:"contact_number"`
	EmailID       string `json:"emailid"`
	// PhotoHash     string `json:"photohash"`
	// DocumentHash  string `json:"dochash"`
}

type BasicInfoDriver struct {
	ID            string          `json:"id"`
	First_Name    string          `json:"firstname"`
	Last_Name     string          `json:"lastname"`
	Gender        string          `json:"gender"`
	DOB           string          `json:"dob"`
	Age           string          `json:"age"`
	ContactNumber string          `json:"contact_number"`
	EmailID       string          `json:"emailid"`
	LicenseNo     string          `json:"license"`
	Crime         string          `json:"crime"`
	VehicleData   []VehiclesOwned `json:"vehicledata"`
	// PhotoHash     string `json:"photohash"`
	// DocumentHash  string `json:"dochash"`
}

type VehiclesOwned struct {
	VehicleType  string `json:"vehicletype"` //prime,sedan,mini
	NumberPlate  string `json:"numberplate"`
	CarCompany   string `json:"carcompany"` //Maruti,etc
	CarMake      string `json:"carmake"`    //800,alto
	CarColour    string `json:"carcolour"`
	ChasisNumber string `json:"chasisnumber"`
	Rto          string `json:"rto"`
}
type TaxiBookedDetails struct {
	ID          string `json:"id"`
	DriverID    string `json:"driverid"` //prime,sedan,mini
	UserID      string `json:"userid"`
	StartLoc    string `json:"startloc"` //Maruti,etc
	EndLoc      string `json:"endloc"`   //800,alto
	DriverName  string `json:"drivername"`
	LicenseNo   string `json:"license"`
	NumberPlate string `json:"numberplate"`

	// UserName   string `json:"username"`
}

// type BasicInfo2 struct {
// 	RelFirstName    string `json:"relfname"`
// 	RelLastName     string `json:"rellname"`
// 	BirthPlace      string `json:"birthplace"`
// 	Nationality     string `json:"nationality"`
// 	EmergencyNumber string `json:"emergency_number"`
// 	BloodGroup      string `json:"bloodgroup"`
// }

// type Address struct {
// 	AddressLine1 string `json:"addressline1"`
// 	AddressLine2 string `json:"addressline2"`
// 	City         string `json:"city"`
// 	Pin          string `json:"pincode"`
// 	State        string `json:"state"`
// }

func main() {
	err := shim.Start(new(SimpleChainCode))
	if err != nil {
		fmt.Printf("Error in starting the simple chaincode: %s", err)
	}
}

func (t *SimpleChainCode) Init(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

func (t *SimpleChainCode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	function, args := stub.GetFunctionAndParameters()
	fmt.Println("The function being invoked is: " + function)

	if function == "BlankRun" {
		return t.BlankRun(stub)
	} else if function == "CreateUserAccount" { //CREATE A NEW ENTRY
		return t.CreateUserAccount(stub, args)
	} else if function == "ReturnUserData" {
		return t.ReturnUserData(stub, args)
	} else if function == "CreateDriverAccount" { //CREATE A NEW ENTRY
		return t.CreateDriverAccount(stub, args)
	} else if function == "AddVehicle" { //CREATE A NEW ENTRY
		return t.AddVehicle(stub, args)
	} else if function == "BookTaxi" { //CREATE A NEW ENTRY
		return t.BookTaxi(stub, args)
	} else if function == "ReturnTaxiData" { //CREATE A NEW ENTRY
		return t.ReturnTaxiData(stub, args)
	}

	fmt.Println("Function not found: " + function)
	strin := "args: "
	for i := 0; i < len(args); i++ {
		strin += args[i]
	}
	fmt.Println(strin)
	return shim.Error("Received unknown function invocation")
}

func (t *SimpleChainCode) BlankRun(stub shim.ChaincodeStubInterface) pb.Response {
	return shim.Success(nil)
}

// id, firstname, lastname, gender, dob, age, contact_number, emailid, photohash, dochash
func (t *SimpleChainCode) CreateUserAccount(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 8 {
		return shim.Error("Incorrect number of arguments. Expecting 8")
	}

	for i := 0; i < 8; i++ {
		if len(args[i]) <= 0 {
			var b bytes.Buffer
			ERR := "Argument " + string(i) + " should be non empty"
			fmt.Println(ERR)
			fmt.Println(i)
			b.WriteString(ERR)
			return shim.Error(b.String())
		}
	}

	// objectType := "basicUserData"
	id := args[0]
	firstname := args[1]
	lastname := args[2]
	gender := args[3]
	dob := args[4]
	age := args[5]
	contact_number := args[6]
	emailid := args[7]

	// photohash := args[8]
	// dochash := args[9]

	var taxiData BasicInfo1
	taxiData.ID = id
	// taxiData = objectType
	taxiData.First_Name = firstname
	taxiData.Last_Name = lastname
	taxiData.Gender = gender
	taxiData.DOB = dob
	taxiData.Age = age
	taxiData.ContactNumber = contact_number
	taxiData.EmailID = emailid

	// taxiData.BasicData_1.PhotoHash = photohash
	// taxiData.BasicData_1.DocumentHash = dochash
	// taxiData.IsActive = "false"

	dataJSONasBytes, err := json.Marshal(taxiData)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(string(id), dataJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}
func (t *SimpleChainCode) AddVehicle(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	if len(args) != 8 {
		return shim.Error("Incorrect number of arguments. Expecting 8")
	}

	for i := 0; i < 8; i++ {
		if len(args[i]) <= 0 {
			var b bytes.Buffer
			ERR := "Argument " + string(i) + " should be non empty"
			fmt.Println(ERR)
			fmt.Println(i)
			b.WriteString(ERR)
			return shim.Error(b.String())
		}
	}
	id := args[0]
	dataAsBytes, err := stub.GetState(id)
	if err != nil {
		return shim.Error("Failed to get user details: " + err.Error())
	} else if dataAsBytes == nil {
		return shim.Error("This user doesn't exist: " + id)
	}

	var taxiData BasicInfoDriver
	err = json.Unmarshal(dataAsBytes, &taxiData) //unmarshal it aka JSON.parse()
	if err != nil {
		return shim.Error(err.Error())
	}

	vehicletype := args[1]
	numberplate := args[2]
	carcompany := args[3]
	carmake := args[4]
	carcolour := args[5]
	chasisnumber := args[6]
	rto := args[7]

	// photohash := args[8]
	// dochash := args[9]
	var vehicledata VehiclesOwned

	vehicledata.VehicleType = vehicletype
	vehicledata.NumberPlate = numberplate
	vehicledata.CarCompany = carcompany
	vehicledata.CarMake = carmake
	vehicledata.CarColour = carcolour
	vehicledata.ChasisNumber = chasisnumber
	vehicledata.Rto = rto

	taxiData.VehicleData = append(taxiData.VehicleData, vehicledata)

	// taxiData.BasicData_1.PhotoHash = photohash
	// taxiData.BasicData_1.DocumentHash = dochash
	// taxiData.IsActive = "false"

	dataJSONasBytes, err := json.Marshal(taxiData)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(string(id), dataJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

func (t *SimpleChainCode) BookTaxi(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var temp, temp2, temp3, temp4 string
	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	for i := 0; i < 4; i++ {
		if len(args[i]) <= 0 {
			ERR := "Argument " + string(i) + " should be non empty"
			return shim.Error(ERR)
		}
	}
	id := "booked_details" + args[1]
	userid := args[1]
	startloc := args[2]
	endloc := args[3]
	fmt.Printf("changes")

	//Fetch any driver
	for i := 1; i < 100; i++ {
		driverid := strconv.Itoa(2)
		logger.Debug("*******************" + driverid)
		//driverid := args[0]
		dataAsBytes, err := stub.GetState(driverid)
		// if err != nil {
		// 	return shim.Error("Failed to get driver details: " + err.Error())
		// } else if dataAsBytes == nil {
		// 	return shim.Error("This driver doesn't exist: " + driverid)
		// }
		//logger.Debug("**************" + stub.GetState)
		if err == nil {
			var driverData BasicInfoDriver
			err = json.Unmarshal(dataAsBytes, &driverData) //unmarshal it aka JSON.parse()
			if err != nil {
				return shim.Error(err.Error())
			}

			//if driverData.VehicleData[0].CarMake == startloc {
			temp = driverData.ID
			temp2 = driverData.LicenseNo
			temp3 = driverData.VehicleData[0].NumberPlate
			temp4 = driverData.First_Name
			break

			//}
		}
	}
	if temp2 == "" {
		ERR := "No driver in " + startloc + " select another area"
		return shim.Error(ERR)
	}

	dataAsBytes1, err := stub.GetState(userid)
	if err != nil {
		return shim.Error("Failed to get user details: " + err.Error())
	} else if dataAsBytes1 == nil {
		return shim.Error("This user doesn't exist: " + userid)
	}

	var taxibook TaxiBookedDetails
	taxibook.ID = "booked_details" + args[1]
	taxibook.DriverID = temp
	taxibook.LicenseNo = temp2
	taxibook.DriverName = temp4
	taxibook.EndLoc = endloc
	taxibook.StartLoc = startloc
	taxibook.UserID = userid
	taxibook.NumberPlate = temp3
	// relfname := args[1]
	// rellname := args[2]
	// pob := args[3]
	// nationality := args[4]
	// emerno := args[5]
	// bg := args[6]

	// uidaiData.BasicData_2.RelFirstName = relfname
	// uidaiData.BasicData_2.RelLastName = rellname
	// uidaiData.BasicData_2.BirthPlace = pob
	// uidaiData.BasicData_2.Nationality = nationality
	// uidaiData.BasicData_2.EmergencyNumber = emerno
	// uidaiData.BasicData_2.BloodGroup = bg

	dataJSONasBytes, err := json.Marshal(taxibook)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(id, dataJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}
func (t *SimpleChainCode) CreateDriverAccount(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	if len(args) != 10 {
		return shim.Error("Incorrect number of arguments. Expecting 15")
	}

	for i := 0; i < 10; i++ {
		if len(args[i]) <= 0 {
			var b bytes.Buffer
			ERR := "Argument " + string(i) + " should be non empty"
			fmt.Println(ERR)
			fmt.Println(i)
			b.WriteString(ERR)
			return shim.Error(b.String())
		}
	}

	// objectType := "basicDriverData"
	id := args[0]
	firstname := args[1]
	lastname := args[2]
	gender := args[3]
	dob := args[4]
	age := args[5]
	contact_number := args[6]
	emailid := args[7]
	license := args[8]
	crime := args[9]
	// vehicletype := args[9]
	// numberplate := args[10]
	// carcompany := args[11]
	// carmake := args[12]
	// carcolour := args[13]
	// chasisnumber := args[14]

	// photohash := args[8]
	// dochash := args[9]

	var taxiData BasicInfoDriver
	taxiData.ID = id
	// taxiData.DocType = objectType
	taxiData.First_Name = firstname
	taxiData.Last_Name = lastname
	taxiData.Gender = gender
	taxiData.DOB = dob
	taxiData.Age = age
	taxiData.ContactNumber = contact_number
	taxiData.EmailID = emailid
	taxiData.LicenseNo = license
	taxiData.Crime = crime
	// taxiData.VehicleData.VehicleType = vehicletype
	// taxiData.VehicleData.NumberPlate = numberplate
	// taxiData.VehicleData.CarCompany = carcompany
	// taxiData.VehicleData.CarMake = carmake
	// taxiData.VehicleData.CarColour = carcolour
	// taxiData.VehicleData.ChasisNumber = chasisnumber

	// taxiData.BasicData_1.PhotoHash = photohash
	// taxiData.BasicData_1.DocumentHash = dochash
	// taxiData.IsActive = "false"

	dataJSONasBytes, err := json.Marshal(taxiData)
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(string(id), dataJSONasBytes)
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)
}

// uid, relfname, rellname, pob, nationality, emerno, bg
// func (t *SimpleChainCode) AddBaseData2(stub shim.ChaincodeStubInterface, args []string) pb.Response {

// 	if len(args) != 7 {
// 		return shim.Error("Incorrect number of arguments. Expecting 7")
// 	}

// 	for i := 0; i < 7; i++ {
// 		if len(args[i]) <= 0 {
// 			ERR := "Argument " + string(i) + " should be non empty"
// 			return shim.Error(ERR)
// 		}
// 	}

// 	id := args[0]
// 	dataAsBytes, err := stub.GetState(id)
// 	if err != nil {
// 		return shim.Error("Failed to get user details: " + err.Error())
// 	} else if dataAsBytes == nil {
// 		return shim.Error("This user doesn't exist: " + id)
// 	}

// 	var taxiData taxiDetails
// 	err = json.Unmarshal(dataAsBytes, &taxiData) //unmarshal it aka JSON.parse()
// 	if err != nil {
// 		return shim.Error(err.Error())
// 	}

// 	relfname := args[1]
// 	rellname := args[2]
// 	pob := args[3]
// 	nationality := args[4]
// 	emerno := args[5]
// 	bg := args[6]

// 	taxiData.BasicData_2.RelFirstName = relfname
// 	taxiData.BasicData_2.RelLastName = rellname
// 	taxiData.BasicData_2.BirthPlace = pob
// 	taxiData.BasicData_2.Nationality = nationality
// 	taxiData.BasicData_2.EmergencyNumber = emerno
// 	taxiData.BasicData_2.BloodGroup = bg

// 	dataJSONasBytes, err := json.Marshal(taxiData)
// 	if err != nil {
// 		return shim.Error(err.Error())
// 	}

// 	err = stub.PutState(id, dataJSONasBytes)
// 	if err != nil {
// 		return shim.Error(err.Error())
// 	}

// 	return shim.Success(nil)
// }

// id, addressline1, addressline2, city, pincode, state
// func (t *SimpleChainCode) AddBaseData3(stub shim.ChaincodeStubInterface, args []string) pb.Response {

// 	if len(args) != 6 {
// 		return shim.Error("Incorrect number of arguments. Expecting 6")
// 	}

// 	for i := 0; i < 6; i++ {
// 		if len(args[i]) <= 0 {
// 			ERR := "Argument " + string(i) + " should be non empty"
// 			return shim.Error(ERR)
// 		}
// 	}

// 	id := args[0]
// 	dataAsBytes, err := stub.GetState(id)
// 	if err != nil {
// 		return shim.Error("Failed to get user details: " + err.Error())
// 	} else if dataAsBytes == nil {
// 		return shim.Error("This user doesn't exist: " + id)
// 	}

// 	var taxiData taxiDetails
// 	err = json.Unmarshal(dataAsBytes, &taxiData) //unmarshal it aka JSON.parse()
// 	if err != nil {
// 		return shim.Error(err.Error())
// 	}

// 	addressline1 := args[1]
// 	addressline2 := args[2]
// 	city := args[3]
// 	pincode := args[4]
// 	state := args[5]

// 	taxiData.AddressData.AddressLine1 = addressline1
// 	taxiData.AddressData.AddressLine2 = addressline2
// 	taxiData.AddressData.City = city
// 	taxiData.AddressData.Pin = pincode
// 	taxiData.AddressData.State = state
// 	taxiData.IsActive = "true"

// 	dataJSONasBytes, err := json.Marshal(taxiData)
// 	if err != nil {
// 		return shim.Error(err.Error())
// 	}
// 	err = stub.PutState(id, dataJSONasBytes)
// 	if err != nil {
// 		return shim.Error(err.Error())
// 	}

// 	return shim.Success(nil)
// }

func (t *SimpleChainCode) ReturnUserData(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}
	detailsAsBytes, err := stub.GetState(args[0])
	if err != nil {
		return shim.Error("Failed to get user details: " + err.Error())
	} else if detailsAsBytes == nil {
		return shim.Error("This user doesn't exist: " + args[0])
	}

	var taxiData BasicInfo1
	err = json.Unmarshal(detailsAsBytes, &taxiData)
	if err != nil {
		fmt.Println("1" + err.Error())
		return shim.Error(err.Error())
	}

	logger.Debug(taxiData)

	detailsAsBytes, err = json.Marshal(taxiData)
	if err != nil {
		fmt.Println("2" + err.Error())
		return shim.Error(err.Error())
	}

	return shim.Success(detailsAsBytes)
}

func (t *SimpleChainCode) ReturnTaxiData(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}
	detailsAsBytes, err := stub.GetState(args[0])
	if err != nil {
		return shim.Error("Failed to get user details: " + err.Error())
	} else if detailsAsBytes == nil {
		return shim.Error("This user doesn't exist: " + args[0])
	}

	var taxiData TaxiBookedDetails
	err = json.Unmarshal(detailsAsBytes, &taxiData)
	if err != nil {
		fmt.Println("1" + err.Error())
		return shim.Error(err.Error())
	}

	detailsAsBytes, err = json.Marshal(taxiData)
	if err != nil {
		fmt.Println("2" + err.Error())
		return shim.Error(err.Error())
	}

	return shim.Success(detailsAsBytes)
}

// func (t *SimpleChainCode) ReturnUserData(stub shim.ChaincodeStubInterface, args []string) pb.Response {
// 	if len(args) != 1 {
// 		return shim.Error("Incorrect number of arguments. Expecting 1")
// 	}

// 	detailsAsBytes, err := stub.GetState(args[0])
// 	if err != nil {
// 		return shim.Error("Failed to get user details: " + err.Error())
// 	} else if detailsAsBytes == nil {
// 		return shim.Error("This user doesn't exist: " + args[0])
// 	}

// 	var licensebase LicenseBase
// 	err = json.Unmarshal(detailsAsBytes, &licensebase)
// 	if err != nil {
// 		fmt.Println("1" + err.Error())
// 		return shim.Error(err.Error())
// 	}

// 	detailsAsBytes, err = json.Marshal(licensebase)
// 	if err != nil {
// 		fmt.Println("2" + err.Error())
// 		return shim.Error(err.Error())
// 	}
// docker exec clitaxi peer chaincode invoke -o orderer.example.com:7050 -C channeltaxi -n fabtaxi -c '{"function":"ReturnUserData","Args":["1"]}'

// 	return shim.Success(detailsAsBytes)
// }
